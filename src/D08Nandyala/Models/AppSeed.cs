﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08Nandyala.Models
{
    public static class AppSeed
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
           
            if (context.Movies.Any())
            {
                return;   // DB already seeded
            }

            var lst1 = new List<Movie>()
            {
                new Movie() {Title="Star wars", ReleaseYear = 1977, RunTime= 121 },
                new Movie() {Title="Inception", ReleaseYear = 2010, RunTime= 148 },
                new Movie() {Title="Toy Story", ReleaseYear = 1995, RunTime= 81 },
            };

            context.Movies.AddRange(lst1);
            context.SaveChanges();
        }
    }
}
