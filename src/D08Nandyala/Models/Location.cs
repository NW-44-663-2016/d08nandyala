﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace D08Nandyala.Models
{
    public class Location
    {
        [ScaffoldColumn(false)]
        [Required]
        public int LocationID { get; set; }
        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "Place")]
        public string Place { get; set; }
        [Display(Name = "Statee")]
        public string State { get; set; }
        [Display(Name = "State Abbreviation")]
        public string StateAbbreviation { get; set; }
        public string County { get; set; }
        [RegularExpression(@" ^\d{5}-\d{4}|\d{5}|[A-Z]\d[A - Z] \d[A - Z]\d$")]
        public string ZipCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
