﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace D08Nandyala.Models
{
    public class ApplicationDbContext : DbContext
    {
       public DbSet<Movie> Movies { get; set; }
       public DbSet<Location> Locations { get; set; }
    }
}
